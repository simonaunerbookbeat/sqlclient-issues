using System;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using WebApplication.Repositories;

namespace TestProject1
{
    public class Tests : Setup
    {
        private WeatherForecastContext _context;
        private WeatherForecastRepository _repo;

        [SetUp]
        public void Setup()
        {
            var optionsBuilder = new DbContextOptionsBuilder<WeatherForecastContext>();
            optionsBuilder.UseSqlServer(ConnectionString);

            _context = new WeatherForecastContext(optionsBuilder.Options);
            _repo = new WeatherForecastRepository(_context);
        }

        [Test]
        public void Test1()
        {
            _repo.GetForecast(DateTime.Now);
        }
    }
}