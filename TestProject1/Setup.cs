using System;
using System.Threading.Tasks;
using System.Transactions;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using WebApplication.Repositories;

namespace TestProject1
{
    [TestFixture]
    public class Setup
    {
        protected async Task Run(Func<IWeatherForecastRepository, WeatherForecastContext, Task> execute)
        {
            var optionsBuilder = new DbContextOptionsBuilder<WeatherForecastContext>();
            optionsBuilder.UseSqlServer(ConnectionString);
            using (var context = new WeatherForecastContext(optionsBuilder.Options))
            {
                using (var ts = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    var repo = new WeatherForecastRepository(context);
                    await execute(repo, context);
                }
            }
        }

        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            try
            {
                // docker run -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=SQLsql123!' -e 'MSSQL_PID=Express' -p 1434:1433 -d mcr.microsoft.com/mssql/server:2019-latest
                ConnectionString =
                    $@"Server=localhost,1434; Database={Guid.NewGuid():N}; User id=sa;Password=SQLsql123!;Encrypt=False;TrustServerCertificate=True;";

                var optionsBuilder = new DbContextOptionsBuilder<WeatherForecastContext>();
                optionsBuilder.UseSqlServer(ConnectionString);
                using (var context = new WeatherForecastContext(optionsBuilder.Options))
                {
                    context.Database.Migrate();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [OneTimeTearDown]
        public void TearDown()
        {
            var optionsBuilder = new DbContextOptionsBuilder<WeatherForecastContext>();
            optionsBuilder.UseSqlServer(ConnectionString);
            using (var context = new WeatherForecastContext(optionsBuilder.Options))
            {
                context.Database.EnsureDeleted();
            }
        }

        public string ConnectionString { get; set; }
    }
}