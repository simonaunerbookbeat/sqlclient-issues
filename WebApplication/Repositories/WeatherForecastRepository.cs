using System;
using System.Linq;

namespace WebApplication.Repositories
{
    public interface IWeatherForecastRepository
    {
        WeatherForecast GetForecast(DateTime dateTime);
    }

    public class WeatherForecastRepository : IWeatherForecastRepository
    {
        private readonly WeatherForecastContext _context;

        public WeatherForecastRepository(WeatherForecastContext context)
        {
            _context = context;
        }

        public WeatherForecast GetForecast(DateTime dateTime)
        {
            return _context.WeatherForecasts.SingleOrDefault(m => m.Date == dateTime.Date);
        }
    }
}