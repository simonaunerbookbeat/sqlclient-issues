using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace WebApplication.Repositories
{
    public class WeatherForecastContext : DbContext
    {
        public WeatherForecastContext(DbContextOptions<WeatherForecastContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Composite primary keys
            modelBuilder.Entity<WeatherForecast>()
                .HasKey(m => new {m.Date});

            modelBuilder.Entity<WeatherForecast>()
                .ToTable("WeatherForecasts");
        }

        public DbSet<WeatherForecast> WeatherForecasts { get; set; }
    }

    public class MetadataDesignTimeContextFactory : IDesignTimeDbContextFactory<WeatherForecastContext>
    {
        public WeatherForecastContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<WeatherForecastContext>();
            var connectionString =
                $@"Server=localhost,1434; Database={Guid.NewGuid():N}; User id=sa;Password=SQLsql123!;Encrypt=False;TrustServerCertificate=True;";
            optionsBuilder.UseSqlServer(connectionString);

            return new WeatherForecastContext(optionsBuilder.Options);
        }
    }
}